# Hard Challenge
Write a program that given the name of a CSV file as input from the command line, containing the list of orders of an ecommerce site, calculates the total of orders for each buyer and prints the total for each buyer. Also calculate, for each customer, the totals by product category. Write the results to a cvs file.
Also write unit tests with at least 70% code coverage.

## Input
The input file is a CSV file with the following structure:

| Id | Article Name | Category | Quantity | Unit Price | Percentage Discount | Buyer |
|-----|--------------|----------|-|-----------|---------------------|------|
|1|Coke|Drink|10|1|0|Mario Rossi|
|2|Coke|Drink|15|2|0|Luca Neri|
|3|Fanta|Drink|5|3|2|Luca Neri|
|4|Chocolate|Food|20|1|10|Mario Rossi|
|5|Wafer|Food|1|4|15|Andrea Bianchi|